﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class XmlManager : MonoBehaviour
{
    private static XmlDocument xmlDocument;
    private static TextAsset textXML;

    public static List<Message> Messages;

    static XmlManager()
    {
        

    }

    public static void LoadXML(string xmlName)
    {
        textXML = Resources.Load("XML/" + xmlName) as TextAsset;

        xmlDocument = new XmlDocument();

        xmlDocument.LoadXml(textXML.ToString());

    }

    public static void LoadMessages()
    {
        string numberMessageValue = xmlDocument.DocumentElement.SelectSingleNode("/Messages/NumberMessages").InnerText;
        int numberMessages = int.Parse(numberMessageValue);

        Messages = new List<Message>();
        Messages.Clear();
        for (int i = 1; i <= numberMessages; i++)
        {
            Message message = new Message();
            message.message = xmlDocument.DocumentElement.SelectSingleNode("/Messages/Message" + i).InnerText;
            Messages.Add(message);
        }
    }
}
