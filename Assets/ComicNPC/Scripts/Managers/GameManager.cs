﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PoolMessageManager poolMessages;

    private void Start()
    {
        XmlManager.LoadXML("Messages");
        XmlManager.LoadMessages();

        poolMessages.Initialize();
        Sensor.IsNear = OnNearNPC;
    }

    private void OnNearNPC(bool isNear, Transform parent)
    {
        int idText = parent.GetComponent<NpcBehavior>().idText;
        poolMessages.MessageControl(isNear, XmlManager.Messages[idText].message, parent);
    }
}
