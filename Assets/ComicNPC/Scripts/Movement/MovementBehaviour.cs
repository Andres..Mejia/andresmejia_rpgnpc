﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehaviour : MonoBehaviour
{
    private RotationBehaviour rotation;
    private Checker checker;

    private Vector3 lastPosition;

    private void Awake()
    {
        checker = GetComponent<Checker>();
        rotation = GetComponent<RotationBehaviour>();
        lastPosition = transform.position;
    }

    private void Update()
    {
        if (checker.GroundChecker())
        {
            lastPosition = transform.position;

            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
            transform.LookAt(transform.position + new Vector3(x, 0, y));

            /*if (checker.ObstacleChecker())
            {
                lastPosition = transform.position;

                float x = Input.GetAxis("Horizontal");
                float y = Input.GetAxis("Vertical");

                transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
                transform.LookAt(transform.position + new Vector3(x, 0, y));
            }else
            {
                transform.position = lastPosition;
            }*/
        }
        else
        {
            transform.position = lastPosition;
        }
        
    }

    
}
