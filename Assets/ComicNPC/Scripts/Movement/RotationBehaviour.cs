﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationBehaviour : MonoBehaviour
{
    public void RotateForward()
    {
        Debug.Log("Active Rotate Forward");
        transform.Rotate(Vector3.right * 90, Space.Self);
    }
}
